package com.josue.powerup.fatory;

import com.josue.powerup.domain.RequestBodyAuth;
import com.josue.powerup.domain.ResponseDto;
import com.josue.powerup.model.User;
import com.josue.powerup.util.PowerUpConstants;
import org.springframework.http.HttpStatus;

import java.util.Locale;

public class FactoryUserDataTest {
    public static User createUserDefault() {
        return new User(
                1L,
                "test name",
                "test lastname",
                "300000000",
                "Test address",
                "test@test.co",
                "*Test_0323"
        );
    }

    public static User createUserWithEmail(String email) {
        return new User(
                1L,
                "test name",
                "test lastname",
                "300000000",
                "Test address",
                email,
                "*Test_0323"
        );
    }

    public static User createUserWithPassword(String password) {
        return new User(
                1L,
                "test name",
                "test lastname",
                "300000000",
                "Test address",
                "test@test.co",
                password
        );
    }

    public static ResponseDto<Void> getResponseWhenEmailOrPasswordIsNotCorrectFormat(String message) {
        return new ResponseDto<>(
                HttpStatus.BAD_REQUEST.value(),
                message,
                "error",
                "40000",
                PowerUpConstants.EMPTY_STRING,
                null
        );
    }

    public static RequestBodyAuth createRequestBodyAuthDefault() {
        return new RequestBodyAuth("test@test.co", "*Test_0323");
    }
}
