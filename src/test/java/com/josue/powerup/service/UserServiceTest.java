package com.josue.powerup.service;

import com.josue.powerup.domain.RequestBodyAuth;
import com.josue.powerup.domain.ResponseDto;
import com.josue.powerup.fatory.FactoryUserDataTest;
import com.josue.powerup.model.User;
import com.josue.powerup.repository.IUserRepository;
import com.josue.powerup.util.PowerUpConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class UserServiceTest {
    @InjectMocks
    UserService userService;

    @Mock
    IUserRepository userRepository;

    @Test
    void mostSaveAUser() {
        //Given
        User user = FactoryUserDataTest.createUserDefault();

        //When
        userService.saveUser(user);

        //Then
        verify(userRepository).save(any(User.class));
    }

    @Test
    void throwPowerUpExceptionWhenEmailIsNotCorrectFormat() {
        //Given
        User user = FactoryUserDataTest.createUserWithEmail("test");
        ResponseDto<Void> expectedResponse = FactoryUserDataTest.
                getResponseWhenEmailOrPasswordIsNotCorrectFormat(PowerUpConstants.MESSAGE_ERROR_FORMAT_EMAIL);

        //When
        ResponseDto<Void> response = userService.saveUser(user);

        //Then
        Assertions.assertEquals(
                response.getUserMessage(),
                expectedResponse.getUserMessage()
        );
    }

    @Test
    void throwPowerUpExceptionWhenPasswordIsNotCorrectFormat() {
        //Given
        User user = FactoryUserDataTest.createUserWithPassword("123");
        ResponseDto<Void> expectedResponse = FactoryUserDataTest.
                getResponseWhenEmailOrPasswordIsNotCorrectFormat(PowerUpConstants.MESSAGE_ERROR_FORMAT_PASSWORD);

        //When
        ResponseDto<Void> response = userService.saveUser(user);

        //Then
        Assertions.assertEquals(
                expectedResponse.getUserMessage(),
                response.getUserMessage()
        );
    }

    @Test
    void getUserByEmailAndPassword() {
        //Given
        User user = FactoryUserDataTest.createUserDefault();
        RequestBodyAuth requestBodyAuth = FactoryUserDataTest.createRequestBodyAuthDefault();

        //When
        when(userRepository.findByEmailAndPassword(requestBodyAuth.getEmail(), requestBodyAuth.getPassword()))
                .thenReturn(Optional.of(user));
        ResponseDto<?> response = userService.getUserByEmailAndPassword(requestBodyAuth);

        //Then
        Assertions.assertEquals("User was found", response.getUserMessage());
    }

    @Test
    void userWasNotFoundByEmailAndPassword() {
        //Given
        RequestBodyAuth requestBodyAuth = FactoryUserDataTest.createRequestBodyAuthDefault();

        //When
        when(userRepository.findByEmailAndPassword(requestBodyAuth.getEmail(), requestBodyAuth.getPassword()))
                .thenReturn(Optional.empty());
        ResponseDto<?> response = userService.getUserByEmailAndPassword(requestBodyAuth);

        //Then
        Assertions.assertEquals("Wrong email or password", response.getUserMessage());
    }
}