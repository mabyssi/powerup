package com.josue.powerup.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "`user`")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user")
    private Long idUser;

    @Column(length = 30)
    @NotBlank
    private String name;

    @Column(length = 50)
    @NotBlank
    private String lastname;

    @Column(length = 20)
    @NotBlank
    private String phone;

    @Column
    @NotBlank
    private String address;

    @Column
    @NotBlank
    private String email;

    @Column(length = 15)
    @NotBlank
    private String password;
}
