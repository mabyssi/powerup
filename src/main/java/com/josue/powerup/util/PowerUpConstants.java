package com.josue.powerup.util;

public class PowerUpConstants {
    public static final String MESSAGE_ERROR_FORMAT_EMAIL = "Error: Format email invalid";
    public static final String MESSAGE_ERROR_FORMAT_PASSWORD = "Error: Password no allowed";
    public static final String EMPTY_STRING = "";
}
