package com.josue.powerup.util;

import com.josue.powerup.exception.PowerUpException;
import org.springframework.http.HttpStatus;

import java.util.regex.Pattern;

public class ValidationUtil {
    private ValidationUtil() {}

    public static void verifyEmailFormat(String email) throws PowerUpException {
        String regex = "[^@]+@[^@]+\\.[a-zA-Z]{2,}";

        if (!Pattern.matches(regex, email)) {
            throw new PowerUpException(
                    PowerUpConstants.MESSAGE_ERROR_FORMAT_EMAIL,
                    PowerUpConstants.MESSAGE_ERROR_FORMAT_EMAIL,
                    HttpStatus.BAD_REQUEST.value()
            );
        }
    }

    public static void verifyPasswordFormat(String password) throws PowerUpException {
        String regex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[*_-])(?=\\S+$).{8,15}$";

        if (!Pattern.matches(regex, password)) {
            throw new PowerUpException(
                    PowerUpConstants.MESSAGE_ERROR_FORMAT_PASSWORD,
                    PowerUpConstants.MESSAGE_ERROR_FORMAT_PASSWORD,
                    HttpStatus.BAD_REQUEST.value()
            );
        }
    }
}
