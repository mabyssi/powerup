package com.josue.powerup.exception;

import lombok.Getter;

@Getter
public class PowerUpException extends Exception {
    private String userMessage;
    private int status;

    public PowerUpException(String developerMessage, String userMessage, int status) {
        super(developerMessage);
        this.userMessage = userMessage;
        this.status = status;
    }
}
