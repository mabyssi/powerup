package com.josue.powerup.service;

import com.josue.powerup.domain.RequestBodyAuth;
import com.josue.powerup.domain.ResponseDto;
import com.josue.powerup.domain.UserResponseDto;
import com.josue.powerup.exception.PowerUpException;
import com.josue.powerup.model.User;
import com.josue.powerup.repository.IUserRepository;
import com.josue.powerup.util.PowerUpConstants;
import com.josue.powerup.util.ValidationUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RequiredArgsConstructor
@Service
@Transactional
public class UserService implements IUserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final IUserRepository userRepository;

    @Override
    public ResponseDto<Void> saveUser(User user) {
        LOGGER.debug("Init UserService.saveUser");
        ResponseDto<Void> response;

        try {
            this.verifyDataUser(user);
            this.userRepository.save(user);

            String message = "User saved";

            response = new ResponseDto<>(
                    HttpStatus.OK.value(),
                    message,
                    message,
                    PowerUpConstants.EMPTY_STRING,
                    PowerUpConstants.EMPTY_STRING,
                    null
            );
        } catch (PowerUpException e) {
            response = new ResponseDto<>(
                    e.getStatus(),
                    e.getUserMessage(),
                    e.getUserMessage(),
                    "40000",
                    PowerUpConstants.EMPTY_STRING,
                    null
            );
        }

        LOGGER.debug("Finish  UserService.saveUser");
        return response;
    }

    @Override
    public ResponseDto<?> getUserByEmailAndPassword(RequestBodyAuth requestBodyAuth) {
        LOGGER.debug("Init UserService.getUserByEmailAndPassword");
        ResponseDto<?> response;
        String message;
        Optional<User> user = this.userRepository.findByEmailAndPassword(requestBodyAuth.getEmail(), requestBodyAuth.getPassword());

        if (user.isEmpty()) {
            message = "Wrong email or password";

            response = new ResponseDto<>(
                    HttpStatus.BAD_REQUEST.value(),
                    message,
                    message,
                    "40001",
                    "Could not find a user with the submitted data",
                    null
            );
        } else {
            message = "User was found";
            User userData = user.get();
            UserResponseDto userResponseDto = new UserResponseDto(
                userData.getIdUser(),
                userData.getName(),
                userData.getLastname(),
                userData.getPhone(),
                userData.getAddress(),
                userData.getEmail()
            );

            response = new ResponseDto<>(
                    HttpStatus.OK.value(),
                    message,
                    message,
                    PowerUpConstants.EMPTY_STRING,
                    PowerUpConstants.EMPTY_STRING,
                    userResponseDto
            );
        }

        LOGGER.debug("Finish UserService.getUserByEmailAndPassword");
        return response;
    }

    private void verifyDataUser(User user) throws PowerUpException {
        LOGGER.debug("Init UserService.verifyDataUser");
        ValidationUtil.verifyEmailFormat(user.getEmail());
        ValidationUtil.verifyPasswordFormat(user.getPassword());
        LOGGER.debug("Finish UserService.verifyDataUser");
    }
}
