package com.josue.powerup.service;

import com.josue.powerup.domain.RequestBodyAuth;
import com.josue.powerup.domain.ResponseDto;
import com.josue.powerup.domain.UserResponseDto;
import com.josue.powerup.model.User;

public interface IUserService {
    ResponseDto<Void> saveUser(User user);

    ResponseDto<?> getUserByEmailAndPassword(RequestBodyAuth requestBodyAuth);
}
