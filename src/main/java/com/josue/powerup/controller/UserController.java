package com.josue.powerup.controller;

import com.josue.powerup.domain.RequestBodyAuth;
import com.josue.powerup.domain.ResponseDto;
import com.josue.powerup.model.User;
import com.josue.powerup.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/user")
public class UserController {
    private final IUserService userService;

    @PostMapping(value = "/save-user")
    public ResponseDto<Void> saveUser(@Valid @RequestBody User user) {
        return userService.saveUser(user);
    }

    @PostMapping(value = "/auth")
    public ResponseDto<?> findUserByEmailAndPassword(@Valid @RequestBody RequestBodyAuth requestBodyAuth) {
        return this.userService.getUserByEmailAndPassword(requestBodyAuth);
    }
}
