package com.josue.powerup.domain;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDto<T> {
    private int status;
    private String userMessage;
    private String developerMessage;
    private String errorCode;
    private String moreInfo;
    private T data;
}
