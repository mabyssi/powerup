package com.josue.powerup.domain;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto {
    private Long idUser;
    private String name;
    private String lastname;
    private String phone;
    private String address;
    private String email;
}
