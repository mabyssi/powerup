package com.josue.powerup.domain;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestBodyAuth {
    @NotBlank
    private String email;
    @NotBlank
    private String password;
}
